#ifndef _SYSTEM_HANDLER_H
#define _SYSTEM_HANDLER_H
#ifdef __cplusplus
extern "C" {
#endif

void Error_Handler (void);

void SystemClock_Config (void);

#ifdef __cplusplus
}
#endif
#endif