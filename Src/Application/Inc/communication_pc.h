#ifndef _COMMUNICATION_PC_
#define _COMMUNICATION_PC_

#include "stdint.h"



#define NUM_RS 3

extern uint8_t request_data[NUM_RS];

extern uint8_t Chip_RL78_initialized[NUM_RS];

extern uint8_t Disconnect_RL78[NUM_RS];


#endif // _COMMUNICATION_PC_

