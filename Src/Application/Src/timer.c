/**
 * @file timer.c
 * @author Tung (nguyenthanhtung8196@gmail.com)
 * @brief
 * @version 1.0
 * @date 24-01-2019
 *
 * @copyright Copyright (c) 2019 Savy
 *
 */
/*Include ----------------------------------------------*/
#include "timer.h"
#include "uart.h"
#include "gpio.h"

static uint8_t err = 0;
static uint32_t err_com = 0;
uint32_t max_loop_read_second;
uint32_t max_loop_write_second;
uint8_t step = 0;
uint8_t debug_number_send;
uint16_t time_template = 0;


void Timer_Init(void)
{
	TIM1_Init();
	TIM2_Init();
	TIM3_Init();
	TIM4_Init();
}
// timer 3
void start_timer3(void)
{
	SEGGER_RTT_printf(0,"start timer \n");
	driver_start_timer(TIM3);
}

void stop_timer3(void)
{
	err = 0;
	SEGGER_RTT_printf(0,"stop timer \n");
	driver_stop_timer(TIM3);
}
// timer 2
void start_timer2(uint16_t time)
{
  time_template = time;
	driver_start_timer(TIM2);
}

void stop_timer2()
{
	driver_stop_timer(TIM2);
}

/*TIMER 1*/
void start_timer1(void)
{
	driver_start_timer(TIM1);
}

void stop_timer1(void)
{
	err_com = 0;
	driver_stop_timer(TIM1);
}
// timer 4
void start_timer4(void)
{
	driver_start_timer(TIM4);
}
	
void stop_timer4(void)
{
	driver_stop_timer(TIM4);
}

void TIM1_UP_IRQHandler()
{
	if (TIM_GetITStatus(TIM1, TIM_IT_Update) != RESET)
	{
		if (err_com == 100)
		{
			step = 0;
			read = 0;
			write = 0;
			SEGGER_RTT_printf(0,"Disconnect COM\n");
			stop_timer2();
			stop_timer1();
		}
		err_com++;
		TIM_ClearITPendingBit(TIM1, TIM_IT_Update);	
	}
}

void TIM3_IRQHandler()
{
	
	if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET)
	{
		if (err == 3)
		{
			send_data2(Disconnect_RL78,NUM_RS);
			err = 0;
			step = 0;
			read = 0;
			write = 0;
			SEGGER_RTT_printf(0,"Disconnect RL78 \n");
			stop_timer2();
			stop_timer3();
		}
		err++;
		TIM_ClearITPendingBit(TIM3, TIM_IT_Update);	
	}
}

void TIM2_IRQHandler()
{
  static uint16_t tg = 0;
  if (TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET)
  {
		
    if (tg >= time_template)
    {		
			time_template = 0;
      tg = 0;
			start_timer4();
			stop_timer2();
    }
		tg ++;
    TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
  }
}

void TIM4_IRQHandler()
{
	static uint16_t loop_read_first = 0;	// 256 loop
	static uint16_t loop_read_second = 0;		// 32 loop
	static uint16_t loop_write_second = 0;	// 8 loop
	static uint16_t loop_write_first = 0;		// 4 loop
  if (TIM_GetITStatus(TIM4, TIM_IT_Update) != RESET)
  {
		if (Chip_RL78 == 1)
		{
			switch (step)
      {
        case 0:
					SEGGER_RTT_printf(0,"0 \n");
					step++;
					My_GPIO_Init();
					GPIO_ResetBits(GPIOA,TX1);
					start_timer2(400);
          break;
        case 1:
					SEGGER_RTT_printf(0,"1 \n");
					GPIO_SetBits(GPIOA,TX1);
					GPIO_SetBits(GPIOB,RESET_RL78);
					step++;
					start_timer2(550);
          break;
        case 2:
					SEGGER_RTT_printf(0,"2 \n");
					GPIO_ResetBits(GPIOA,TX1);
					GPIO_ResetBits(GPIOB,RESET_RL78);
					step++;
					start_timer2(6);
          break;
        case 3:
					SEGGER_RTT_printf(0,"3 \n");
					GPIO_SetBits(GPIOB,RESET_RL78);
					step++;
					start_timer2(6);
          break;
        case 4:
					step++;
					SEGGER_RTT_printf(0,"4 \n");
					GPIO_SetBits(GPIOA,TX1);
					all_Uart_Init();
					start_timer2(1);				
          break;
        case 5:
					SEGGER_RTT_printf(0,"5 \n");
					send_data1(one_wire,NUM_ONE_WIRE);
					step++;
					start_timer2(3);
          break;
        case 6:
					step ++;
					SEGGER_RTT_printf(0,"6 \n");
					send_data1(baud_rate,NUM_BAUD_RATE);
					start_timer2(30);
          break;
        case 7:
					SEGGER_RTT_printf(0,"7 \n");
					step++;
					send_data1(reset_cm,NUM_RESET);
					start_timer2(1);
          break; 
        case 8:
					SEGGER_RTT_printf(0,"8 \n");
					step++;
					send_data1(read_info_cm, NUM_READ_INFO);
					start_timer2(20);
          break;
				case 9:
					SEGGER_RTT_printf(0,"done RL78 \n");
					Chip_RL78 = 0;
					step = 0;
					break;
			}
		}
		
    if (read == 1)
    {
      switch (step)
      {
        case 0:
					loop_read_first = 0;
					loop_read_second = 0;
					SEGGER_RTT_printf(0,"0 \n");
					step++;
					My_GPIO_Init();
					set_data();
					GPIO_ResetBits(GPIOA,TX1);
					start_timer2(400);
          break;
        case 1:
					SEGGER_RTT_printf(0,"1 \n");
					GPIO_SetBits(GPIOA,TX1);
					GPIO_SetBits(GPIOB,RESET_RL78);
					step++;
					start_timer2(550);
          break;
        case 2:
					SEGGER_RTT_printf(0,"2 \n");
					GPIO_ResetBits(GPIOA,TX1);
					GPIO_ResetBits(GPIOB,RESET_RL78);
					step++;
					start_timer2(6);
          break;
        case 3:
					SEGGER_RTT_printf(0,"3 \n");
					GPIO_SetBits(GPIOB,RESET_RL78);
					step++;
					start_timer2(6);
          break;
        case 4:
					step++;
					SEGGER_RTT_printf(0,"4 \n");
					GPIO_SetBits(GPIOA,TX1);
					all_Uart_Init();
					start_timer2(1);				
          break;
        case 5:
					SEGGER_RTT_printf(0,"5 \n");
					send_data1(one_wire,NUM_ONE_WIRE);
					step++;
					start_timer2(3);
          break;
        case 6:
					SEGGER_RTT_printf(0,"6 \n");
					send_data1(baud_rate,NUM_BAUD_RATE);
					step++;
					start_timer2(30);
          break;
        case 7:
					SEGGER_RTT_printf(0,"7 \n");
					send_data1(reset_cm,NUM_RESET);
					step++;
					start_timer2(1);
          break; 
        case 8:
					step++;
					SEGGER_RTT_printf(0,"8 \n");
					send_data1(read_info_cm, NUM_READ_INFO);			
          break;
				case 9:
					step++;
					set_byte(data_read, NUM);
					start_timer2(2);
					break;
				case 10:
					start_timer3();
					step++;
					send_data1(check_sum_cm, NUM_CHECK_SUM);
					break;
				case 11:
					stop_timer3();
					step ++;
					send_data1(write_cm, NUM_WRITE);
					start_timer2(3);
					break;
				case 12:
					step ++;
					data_read[0] = 0x02;
					data_read[1] = 0x00;
					data_read[loop_read_first +2] = 0x00;
					data_read[258] = cal_ck_1(data_read, NUM);
					data_read[259] = 0x03;
					send_data1(data_read, NUM);
					start_timer2(20);
					break;
				case 13:
					loop_read_first++;
					if (loop_read_first < 256)
					{
						step = 10;
					}
					else 
					{
						step ++;
						loop_read_first = 0;
					}
					start_timer2(1);
					break;
				case 14:
					start_timer3();
					step++;
					send_data1(check_sum_cm, NUM_CHECK_SUM);
					break;
				case 15:
					stop_timer3();
					loop_read_second++;
					check_sum_cm[4]++;
					check_sum_cm[7]++;
					check_sum_cm[9] = cal_ck_2(check_sum_cm, NUM_CHECK_SUM);
					write_cm[4] ++;
					write_cm[7] ++;
					write_cm[9] = cal_ck_2(write_cm, NUM_WRITE);
					if ( loop_read_second < max_loop_read_second)
					{
						step = 9;
					}
					else
					{
						step ++;
					}
					start_timer2(9);
					break;
				case 16:
					SEGGER_RTT_printf(0,"done read \n");
					index_data = 0;
					step = 0;
					read = 0;
					loop_read_first = 0;
					loop_read_second = 0;
					USART_Cmd(USART1,DISABLE);
					break;;
      }
			stop_timer4();
    }
		if (write == 1)
		{
			switch (step)
      {
        case 0:
					loop_write_first = 0;
					loop_write_second = 0;
					SEGGER_RTT_printf(0,"0 \n");
					debug_number_send = 0;
					step++;
					My_GPIO_Init();
					set_data();
					GPIO_ResetBits(GPIOB,RESET_RL78);
					GPIO_ResetBits(GPIOA,TX1);
					start_timer2(400);
          break;
        case 1:
					SEGGER_RTT_printf(0,"1 \n");
					step++;
					GPIO_SetBits(GPIOA,TX1);
					GPIO_SetBits(GPIOB,RESET_RL78);
					start_timer2(550);
          break;
        case 2:
					SEGGER_RTT_printf(0,"2 \n");
					step++;
					GPIO_ResetBits(GPIOA,TX1);
					GPIO_ResetBits(GPIOB,RESET_RL78);
					start_timer2(6);
          break;
        case 3:
					SEGGER_RTT_printf(0,"3 \n");
					step++;
					GPIO_SetBits(GPIOB,RESET_RL78);
					start_timer2(6);
          break;
        case 4:
					SEGGER_RTT_printf(0,"4 \n");
					step++;
					GPIO_SetBits(GPIOA,TX1);
					all_Uart_Init();
					start_timer2(1);
          break;
        case 5:
					SEGGER_RTT_printf(0,"5 \n");
					step++;
					send_data1(one_wire,NUM_ONE_WIRE);
					start_timer2(3);
          break;
        case 6:
					SEGGER_RTT_printf(0,"6 \n");
					step++;
					send_data1(baud_rate,NUM_BAUD_RATE);
					start_timer2(30);
          break;
        case 7:
					SEGGER_RTT_printf(0,"7 \n");
					step++;
					send_data1(reset_cm,NUM_RESET);
					start_timer2(1);
          break; 
        case 8:
					SEGGER_RTT_printf(0,"8 \n");
					step++;
					send_data1(read_info_cm, NUM_READ_INFO);
          break;
				case 9:
					step++;
					send_data1(blank_check_cm, NUM_BLANK_CHECK);
					start_timer2(3);
					break;
				case 10:
					step ++;
					send_data1(erase_cm, NUM_ERASE);
					start_timer3();
				case 11:
					step++;
					debug_number_send++;
					send_data2(request_data,NUM_RS);
					SEGGER_RTT_printf(0,"%d \n",debug_number_send);
					start_timer1();			
					break;
				case 12:
					step++;
					send_data1(write_cm, NUM_WRITE);
					start_timer2(1);
					break;
				case 13:
					loop_write_first++;
					write_data[0] = 0x02;
					write_data[1] = 0x00;
					write_data[259] = 0x03;
					write_data[258] = cal_ck_1(write_data, NUM);
					send_data1(write_data, NUM);
					write_cm[4]++;
					write_cm[7]++;
					write_cm[9] = cal_ck_2(write_cm, NUM_WRITE);
					if ( loop_write_first < 4 )
					{
						step = 11;
					}
					else
					{
						step++;
						loop_write_first=0;
					}
					start_timer3();
					break;
				case 14:
					loop_write_second++;
					blank_check_cm[4] = blank_check_cm[4] + 4;
					blank_check_cm[7] = blank_check_cm[7] + 4;
					blank_check_cm[10] = cal_ck_2(blank_check_cm, NUM_BLANK_CHECK);
					erase_cm[4] = erase_cm[4] + 4;
					erase_cm[6] = cal_ck_2(erase_cm, NUM_ERASE);
					if (loop_write_second < max_loop_write_second)
					{
						step = 9;
					}
					else 
					{
						step ++;
					}
					
					start_timer2(1);
					break;
				case 15:
					SEGGER_RTT_printf(0,"done write \n");
					step = 0;
					write = 0;
					loop_write_second = 0 ;
					loop_write_first = 0;
					break;
      }
			stop_timer4();
		}
		stop_timer4();
  }
}


