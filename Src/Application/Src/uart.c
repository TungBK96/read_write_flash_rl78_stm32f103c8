/**
 * @file uart.c
 * @author Tung (nguyenthanhtung8196@gmail.com)
 * @brief
 * @version 1.0
 * @date 24-01-2019
 *
 * @copyright Copyright (c) 2019 Savy
 *
 */
/*Include ----------------------------------------------*/
#include "uart.h"
#include "gpio.h"
#include "timer.h"

static uint8_t RX_buff[300];
static uint8_t RX_received[300];
static uint8_t data_flash[258];
static uint16_t debug = 0;
uint8_t write = 0;
uint8_t read = 0;
uint8_t Chip_RL78 = 0;
static uint32_t end_addresses_flash;
uint32_t start_addresses_flash = 0x000f1000;
uint16_t compare;
uint8_t result;
uint16_t index_rx1;
uint16_t index_rx2;
uint16_t count = 0;
uint32_t index_data = 0;
uint8_t Chip_RL78_initialized[NUM_RS];

void Uart_Init(void)
{
	USART2_Init(RX2,TX2);
}

void all_Uart_Init(void)
{
	USART1_Init(RX1,TX1);
	USART2_Init(RX2,TX2);
}

void USART2_IRQHandler(void)
{
  if ((USART2->SR & USART_FLAG_RXNE) != RESET)
  {
    RX_received[index_rx2] = USART_ReceiveData(USART2);
    //SEGGER_RTT_printf(0,"RX_received[%d] = 0x%x -",index_rx2,RX_received[index_rx2]);
		if (step == 0)
		{
			if ((RX_received[index_rx2 - 2] == 0xff) && (RX_received[index_rx2 - 1] == 0x00) && (RX_received[index_rx2] == 0x01)
					&& (read == 0) && (write == 0))
			{
				SEGGER_RTT_printf(0, "read \n");
				end_addresses_flash = 0;
				count = 0;
				memset(data_flash,0,258);
				index_rx1 = 0;
				index_data = 0;
				compare = 0;
				result = 0;
				debug = 0;
				read = 1;
				step = 0;
				start_timer2(1);
				index_rx2 = 0;
			}
			if ((RX_received[index_rx2 - 2] == 0xff) && (RX_received[index_rx2 - 1] == 0x01) && (RX_received[index_rx2] == 0x00)
					&& (write == 0) && (read == 0))
			{
				SEGGER_RTT_printf(0, "write \n");
				end_addresses_flash = 0;
				index_rx1 = 0;
				index_data = 0;
				compare = 0;
				result = 0;
				write = 1;
				step = 0;
				start_timer2(1);
				index_rx2 = 0;
			}
			if ((RX_received[index_rx2 - 2] == 0xff) && (RX_received[index_rx2 - 1] == 0x0f) && (RX_received[index_rx2] == 0xf2) &&(Chip_RL78 == 0))
			{
				SEGGER_RTT_printf(0, "initialize chip RL78 \n");
				max_loop_write_second = 0;
				end_addresses_flash = 0;
				Chip_RL78 = 1;
				index_rx1 = 0;
				step = 0;
				start_timer2(1);
				index_rx2 = 0;
			}
		}
		if (read == 1)
		{
				index_rx2 = 0;
				SEGGER_RTT_printf(0,"stop stop stop stop stop stop \n");
				stop_timer1();
		}
		if (write == 1)
		{
			if (step == 12)
			{
				stop_timer1();
				if (2 <= index_rx2 <= 257)
				{
					write_data[index_rx2] = RX_received[index_rx2];
				}
				if (index_rx2 == 258)
				{
					index_rx2 = 0;
					start_timer2(1);
				}
			}
		}
    index_rx2++;
  }
}

void USART1_IRQHandler(void)
{
	GPIO_ResetBits(GPIOA,LED_RL78_RX);
	GPIO_ResetBits(GPIOB,LED_RL78_TX);
  if ((USART1->SR & USART_FLAG_RXNE) != RESET)
  {
		if (Chip_RL78 == 1)
		{
			 RX_buff[index_rx1] = USART_ReceiveData(USART1);
				//SEGGER_RTT_printf(0,"0x%x \n",RX_buff[index_rx1]);
			 if ((RX_buff[index_rx1] == 0x03) && (RX_buff[index_rx1 - 5] == 0x03)
           && (RX_buff[index_rx1 - 6] == 0x02))
        {
					index_rx1 = 0;
        }			
				if (step == 9)
				{
					if ((RX_buff[index_rx1] == 0x03)&&(RX_buff[index_rx1- 24] == 0x16)&&(RX_buff[index_rx1- 25] == 0x02))
					{
						//SEGGER_RTT_printf(0,"0x%x 0x%x 0x%x \n",RX_buff[index_rx1-5],RX_buff[index_rx1-6],RX_buff[index_rx1-7]);
						end_addresses_flash = RX_buff[index_rx1-5];
						end_addresses_flash = (end_addresses_flash << 8) +  RX_buff[index_rx1-6];
						end_addresses_flash = (end_addresses_flash << 8) + RX_buff[index_rx1-7];				
						max_loop_read_second = (end_addresses_flash - start_addresses_flash +1)/256;					
						max_loop_write_second = max_loop_read_second/4;		
						SEGGER_RTT_printf(0," max_loop_write_second = %d \n",max_loop_write_second);
						index_rx1 = 0;
						Chip_RL78_initialized[0] = 0x11;
						Chip_RL78_initialized[1] = max_loop_read_second;
						Chip_RL78_initialized[2] = cal_ck_3(Chip_RL78_initialized, NUM_RS);
						send_data2(Chip_RL78_initialized, NUM_RS);
					}
				}
				index_rx1++;
		}
    if (read == 1)
    {		
      RX_buff[index_rx1] = USART_ReceiveData(USART1);
			if (step == 8)
			{
				index_rx1 = 0;
			}
			if (step == 9)
			{
				//SEGGER_RTT_printf(0,"RX_buff[%d] = 0x%x \n",index_rx1, RX_buff[index_rx1]);
				if ((RX_buff[index_rx1] == 0x03)&&(RX_buff[index_rx1- 24] == 0x16)&&(RX_buff[index_rx1- 25] == 0x02))
				{				
					end_addresses_flash = RX_buff[index_rx1-5];
					end_addresses_flash = (end_addresses_flash << 8) +  RX_buff[index_rx1-6];
					end_addresses_flash = (end_addresses_flash << 8) + 	RX_buff[index_rx1-7];
					max_loop_read_second = (end_addresses_flash - start_addresses_flash +1)/256;
					max_loop_write_second = max_loop_read_second/4;
					SEGGER_RTT_printf(0," max_loop_write_second = %d \n",max_loop_write_second);
					index_rx1 = 0;
					start_timer2(3);
				}
			}
      if (step == 11 || step == 15)
      {
        if ((RX_buff[index_rx1] == 0x03) && (RX_buff[index_rx1 - 4] == 0x02)
            && (RX_buff[index_rx1 - 5] == 0x02) && (index_data == 0))
        {
          index_data = 1;
          compare = RX_buff[index_rx1 - 3] ;
          index_rx1 = 0;
          memset(RX_buff, 0, 300);
					start_timer2(2);
        }
        if ((RX_buff[index_rx1] == 0x03) && (RX_buff[index_rx1 - 4] == 0x02)
            && (RX_buff[index_rx1 - 5] == 0x02) && (index_data != 0))
        {
          //SEGGER_RTT_printf(0,"0x%x \n",result);
          SEGGER_RTT_printf(0, "%d \n", debug);
          result = RX_buff[index_rx1 - 3] - compare;
          data_flash[count + 1] = result;
          debug++;
          count++;
          if (count == 256)
          {
            data_flash[0] = 0x11;
            data_flash[257] = cal_ck_3(data_flash, 258);
            send_data2(data_flash, 258);
            count = 0;
            index_data = 0;
            memset(data_flash, 0, 258);
						start_timer1();
          }
          compare = RX_buff[index_rx1 - 3];
          index_rx1 = 0;
          memset(RX_buff, 0, 300);
					start_timer2(2);
        }
      }
			index_rx1++;
    }	
		if (write == 1)
		{
			RX_buff[index_rx1] = USART_ReceiveData(USART1);
				if (step == 9)
				{
					if ((RX_buff[index_rx1] == 0x03)&&(RX_buff[index_rx1- 24] == 0x16)&&(RX_buff[index_rx1- 25] == 0x02))
					{
						end_addresses_flash = RX_buff[index_rx1-5];
						end_addresses_flash = (end_addresses_flash << 8) +  RX_buff[index_rx1-6];
						end_addresses_flash = (end_addresses_flash << 8) + 	RX_buff[index_rx1-7];
						max_loop_read_second = (end_addresses_flash - start_addresses_flash +1)/256;
						max_loop_write_second = max_loop_read_second/4;
						SEGGER_RTT_printf(0," max_loop_write_second = %d \n",max_loop_write_second);
						index_rx1 = 0;
						start_timer2(30);
					}
				}
				if ((RX_buff[index_rx1] == 0x03) && (RX_buff[index_rx1 - 4] == 0x02)&& (RX_buff[index_rx1 - 5] == 0x02))
				{
					start_timer2(10);
					index_rx1 = 0;
					stop_timer3();
				}
			index_rx1++;
		}
  }
}

