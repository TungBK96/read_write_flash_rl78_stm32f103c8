#ifndef _GPIO_H
#define _GPIO_H

#include "config_gpio.h"

/**
 * @brief Init GPIO
 *
 */
void gpio_init (void);

#endif  // _GPIO_H
