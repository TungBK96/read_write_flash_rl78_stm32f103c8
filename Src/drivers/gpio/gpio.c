
#include "drivers/gpio/gpio.h"
#include "mx_gpio.h"
 
typedef struct 
{
    GPIO_TypeDef gpio_port;
    uint8_t      gpio_pin;
    uint8_t       node;
} gpio_handler_t;


/**
 * @brief Init GPIO
 *
 */
void gpio_init (void)
{
    MX_GPIO_Init ();
}
