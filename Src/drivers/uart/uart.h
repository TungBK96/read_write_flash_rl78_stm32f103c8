
#ifndef _UART_H
#define _UART_H
#ifdef __cplusplus
extern "C" {
#endif

#include "stm32f1xx_hal.h"
#include <status-codes.h>

extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;

typedef UART_HandleTypeDef uart_handler;

/**
 * @brief Init UART1
 *
 * @param baud Config UART baud rate
 * @param stop_bit Config UART stopbit
 */
void uart_init_port1 (int baud, int stop_bit);

/**
 * @brief Init UART2
 *
 * @param baud Config UART baud rate
 * @param stop_bit Config UART stopbit
 */
void uart_init_port2 (int baud, int stop_bit);

/**
 * @brief Write data to UART bus
 *
 * @param huart UART handler
 * @param data data to write on UART bus
 * @param len Size of data
 */
enum status_code uart_write (uart_handler *huart, uint8_t *data, int len);

/**
 * @brief Read data from UART bus
 *
 * @param huart UART handler
 * @param data data read from UART bus
 * @param len Size of data
 */
enum status_code uart_read (uart_handler *huart, uint8_t *data, int len);

#ifdef __cplusplus
}
#endif
#endif /*_UART_H */
