/**
 * @file uart.c
 * @author TienNV (ngocviettien@ginno.com)
 * @brief
 * @version 1.0
 * @date 14-01-2019
 *
 * @copyright Copyright (c) 2019 Ginnovations
 *
 */

#include "mx_usart.h"

#include "uart.h"
#include <status-codes.h>

#define UART_TIMEOUT 1000

/**
 * @brief Init UART1
 *
 * @param baud Config UART baud rate
 * @param stop_bit Config UART stopbit
 */
void uart_init_port1 (int baud, int stop_bit)
{
    MX_USART1_UART_Init (baud, stop_bit);
}

/**
 * @brief Init UART2
 *
 * @param baud Config UART baud rate
 * @param stop_bit Config UART stopbit
 */
void uart_init_port2 (int baud, int stop_bit)
{
    MX_USART2_UART_Init (baud, stop_bit);
}

/**
 * @brief Write data to UART bus
 *
 * @param huart UART handler
 * @param data data to write on UART bus
 * @param len Size of data
 */
enum status_code uart_write (uart_handler *huart, uint8_t *data, int len)
{    
//	  for(uint8_t i =0 ;i< len;i++)
//	  {
//	    printf("data[%d] = %02X\n",i,*(data+i));
//	  }
    if (HAL_UART_Transmit (huart,data, len, UART_TIMEOUT) != HAL_OK)
    {
        return STATUS_ERROR;
    }

    return STATUS_OK;
}

/**
 * @brief Read data from UART bus
 *
 * @param huart UART handler
 * @param data data read from UART bus
 * @param len Size of data
 */
enum status_code uart_read (uart_handler *huart, uint8_t *data, int len)
{
    if (HAL_UART_Receive (huart, data, len, UART_TIMEOUT) != HAL_OK)
    {
        return STATUS_ERROR;
    }

    return STATUS_OK;
}

