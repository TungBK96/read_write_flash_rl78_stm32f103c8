#include "rl78_type.h"
#include "status-code.h"
#include "stdint.h"
#include "drivers/uart/uart.h"

uint32_t RL78_interface_init()
{
    // khoi tao uart 
}
uint32_t RL78_read_data(uint8_t *data)
{
    status_code_t status;

    status = uart_read (&fp_handler, data, 1);
    if (status == STATUS_OK)
    {
        return RL78_OK;
    }

    return RL78_DEVICE_TIMEOUT_ERROR;
}
uint32_t RL78_write_data(uint8_t *data,uint32_t len)
{
  status_code_t status;
  status = uart_write(&fp_handler,data,len);
  if (status == STATUS_OK)
  {
      return RL78_OK;
  }
  return RL78_DEVICE_TIMEOUT_ERROR;
}


