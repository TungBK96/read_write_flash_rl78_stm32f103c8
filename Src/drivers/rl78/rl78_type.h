#ifndef __RL78_TYPE_H
#define __RL78_TYPE_H
#ifdef __cplusplus
extern "C" {
#endif


#define RL78_OK                                   (0)
#define RL78_DEVICE_OTHER_ERROR                   (0x11)
#define RL78_DEVICE_OPEN_ERROR                    (0x12)
#define RL78_DEVICE_CONFIGURE_ERROR               (0x13)
#define RL78_DEVICE_POLL_ERROR                    (0x14)
#define RL78_DEVICE_TIMEOUT_ERROR                 (0x15)
#define RL78_DEVICE_READ_ERROR                    (0x16)
#define RL78_DEVICE_WRITE_ERROR                   (0x17)
#define RL78_ACTION_BUFFER_IS_NULL                (0x31)

#ifdef __cplusplus
}
#endif
#endif