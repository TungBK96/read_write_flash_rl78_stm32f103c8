#ifndef __CONFIG_CHIPFLASH_H
#define __CONFIG_CHIPFLASH_H
#ifdef __cplusplus
extern "C" {
#endif
#define RESRL78 0
#define NXP9S12  1

#define CHIPFLASH_MODEL RESRL78

#if CHIPFLASH_MODEL == RESRL78

#define fp_handler      huart1
#define RL78_PORT     1
#define RL78_BAUD     115200
#define RL78_STOPBIT  1

#elif CHIPFLASH_MODEL == NXP9S12
#define fp_handler 		huart1
#define NXP9S12_PORT		1
#define NXP9S12_BAUD		115200
#define NXP9S12_STOPBIT   1
#endif

#ifdef __cplusplus
}
#endif
#endif