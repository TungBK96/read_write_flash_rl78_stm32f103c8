#ifndef STATUS_CODES_H_INCLUDED
#define STATUS_CODES_H_INCLUDED

// clang-format off

/**
 * Status code that may be returned by shell commands and protocol
 * implementations. The value returned will be the negative of these
 * codes.
 *
 * \note Any change to these status codes and the corresponding
 * message strings is strictly forbidden. New codes can be added,
 * however, but make sure that any message string tables are updated
 * at the same time.
 */
typedef enum status_code {
    STATUS_OK               =   0,    /**< Success */
    STATUS_INVALID_PARAM    =   1,    /**< Invalid argument */
    STATUS_PROTO_ERROR      =   2,    /**< Protocol error */
    STATUS_READ_FAILED      =   3,    /**< Read operation failed */
    STATUS_WRITE_FAILED     =   4,    /**< Write operation failed */
    STATUS_INSUFF_RESOURCES =   5,    /**< Insufficient resources */
    STATUS_BAD_ADDRESS      =   6,    /**< Bad address */
    STATUS_INVALID_IMAGE    =   7,    /**< Image format error */
    STATUS_UNSUPP_TAG       =   8,    /**< Unsupported tag in image */
    STATUS_INTERNAL_ERROR   =   9,  /**< Internal error */
    STATUS_BAD_COMMAND      =  10,  /**< No such command */
    STATUS_TIMEOUT          =  11,  /**< Operation timed out */
    STATUS_UNSUPP_CHIP      =  12,  /**< Unsupported chip */
    STATUS_IO_ERROR         =  13,    /**< I/O error */
    STATUS_CORRUPTED_IMAGE  =  14,    /**< Image is corrupted */
    STATUS_ERASE_FAILED     =  15,  /**< Erase operation failed */
    STATUS_FLUSHED          =  16,  /**< Request flushed from queue */
    STATUS_ERROR            =  17,    /**< Unknow error */
} status_code_t;

/**
 * \brief Operation in progress.
 *
 * This status code is for driver-internal use when an operation is
 * currently being performed.
 *
 * \note Drivers should never return this status code to any callers.
 * It is strictly for internal use.
 */
#define STATUS_IN_PROGRESS    1024    /**< Operation in progress */

// clang-format on

#endif /* STATUS_CODES_H_INCLUDED */
