/**
 * @file driver_timer.c
 * @author Tung (nguyenthanhtung8196@gmail.com)
 * @brief
 * @version 1.0
 * @date 24-01-2019
 *
 * @copyright Copyright (c) 2019 Savy
 *
 */
/*Include ------------------------------------------------------*/
#include "driver_timer.h"
#include "driver_uart.h"

/**
  * @brief  Initializes the Timer 1.
  * @param  None.
  * @retval None.
  */
void TIM1_Init()
{
	TIM_TimeBaseInitTypeDef TIMER_InitStructure;
  NVIC_InitTypeDef NVIC_InitStructure;

  RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);

  TIM_TimeBaseStructInit(&TIMER_InitStructure);
  TIMER_InitStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIMER_InitStructure.TIM_Prescaler = 7199;
  TIMER_InitStructure.TIM_Period = 100 ;
  TIM_TimeBaseInit(TIM1, &TIMER_InitStructure);
  TIM_ITConfig(TIM1, TIM_IT_Update, ENABLE);
  /* NVIC Configuration */
  /* Enable the TIM1_IRQn Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = TIM1_UP_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
}

/**
  * @brief  Initializes the Timer 2.
  * @param  None.
  * @retval None.
  */
void TIM2_Init(void)
{
  TIM_TimeBaseInitTypeDef TIMER_InitStructure;
  NVIC_InitTypeDef NVIC_InitStructure;

  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

  TIM_TimeBaseStructInit(&TIMER_InitStructure);
  TIMER_InitStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIMER_InitStructure.TIM_Prescaler = 7199;
  TIMER_InitStructure.TIM_Period = 9 ;
  TIM_TimeBaseInit(TIM2, &TIMER_InitStructure);
  TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
  /* NVIC Configuration */
  /* Enable the TIM2_IRQn Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
}

/**
  * @brief  Initializes the Timer 3.
  * @param  None.
  * @retval None.
  */
void TIM3_Init(void)
{
	TIM_TimeBaseInitTypeDef TIMER_InitStructure;
  NVIC_InitTypeDef NVIC_InitStructure;

  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);

  TIM_TimeBaseStructInit(&TIMER_InitStructure);
  TIMER_InitStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIMER_InitStructure.TIM_Prescaler = 7199;
  TIMER_InitStructure.TIM_Period = 10000;
  TIM_TimeBaseInit(TIM3, &TIMER_InitStructure);
  TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);
  /* NVIC Configuration */
  /* Enable the TIM3_IRQn Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
}

/**
  * @brief  Initializes the Timer 4.
  * @param  None.
  * @retval None.
  */
void TIM4_Init(void)
{
  TIM_TimeBaseInitTypeDef TIMER_InitStructure;
  NVIC_InitTypeDef NVIC_InitStructure;

  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);

  TIM_TimeBaseStructInit(&TIMER_InitStructure);
  TIMER_InitStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIMER_InitStructure.TIM_Prescaler = 7199;
  TIMER_InitStructure.TIM_Period = 1;
  TIM_TimeBaseInit(TIM4, &TIMER_InitStructure);
  TIM_ITConfig(TIM4, TIM_IT_Update, ENABLE);

  /* NVIC Configuration */
  /* Enable the TIM4_IRQn Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = TIM4_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
}

/**
  * @brief  Start timer .
	* @param  TIMx: Where x can be (1..4) to select TIMER peripheral.
  * @retval None.
  */
void driver_start_timer(TIM_TypeDef* TIMx)
{
	TIM_Cmd(TIMx, ENABLE);
}

/**
  * @brief  Stop timer .
	* @param  TIMx: Where x can be (1..4) to select TIMER peripheral.
  * @retval None.
  */
void driver_stop_timer(TIM_TypeDef* TIMx)
{
	TIM_Cmd(TIMx, DISABLE);
	TIM_ClearITPendingBit(TIMx, TIM_IT_Update);
}
