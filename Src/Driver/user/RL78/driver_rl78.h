/**
 * @file driver_rl78.h
 * @author Tung (nguyenthanhtung8196@gmail.com)
 * @brief
 * @version 1.0
 * @date 24-01-2019
 *
 * @copyright Copyright (c) 2019 Savy
 *
 */
 
#ifndef _DRIVER_RL78_
#define _DRIVER_RL78_

#ifdef __cplusplus
extern "C" {
#endif
#include "stdint.h"

typedef struct
{
	uint8_t read;
	uint8_t write;
	uint8_t Chip_RL78;
}RL78_action_t;
	
typedef struct
{
	uint32_t end_addresses_flash;
	uint32_t start_addresses_flash;
}flash_memory_t;

	
#ifdef __cplusplus
}
#endif

#endif

